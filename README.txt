Address Field UK County
  by James Forbes Keir, james@website-express.co.uk
-----------------------------------------

Description:
-----------------------------------------
This module allows the administrative area field to show as County for UK addresses.

This information is stored with the other Address Field data.

Requirements:
-----------------------------------------
As this module is an add-on to Address Field, it requires the 
Address Field module. https://www.drupal.org/project/addressfield

Installation:
-----------------------------------------
Install in the usual manner:
- Unpack the module folder into your modules directory
- Go to /admin/modules and enable.

Configuration:
-----------------------------------------
None - enabling it is enough.

The label can be changed using your preferred localisation method.
For single language sites, add the string override as directed in 
settings.php or use the string override module.

Views
-----------------------------------------
The Addressfield module has built-in support for displaying the
sub-administrative area.

Credits
-----------------------------------------
Based on code from RickJ in the following Address Field issue:
https://www.drupal.org/project/addressfield/issues/3019397

